# Linked Open Data Crawler

Implemented according to the [Acropolis design document](https://bbcarchdev.github.io/inside-acropolis/) - currently pretending to be an `Anansi` processor (ie queuing all S/P/O URIs blindly.)

There is currently no queue persistence or storage of the downloaded RDF.

(I'm not sure it'll run as-is because of the `rjp.is/golibrdf` requirement but that's something to fix later.)
