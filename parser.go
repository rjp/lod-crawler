package main

import (
    "fmt"
    "log"

    "rjp.is/golibrdf"
)

type librdfContext struct {
    world *golibrdf.World
    storage *golibrdf.Storage
    uri *golibrdf.Uri
    parser *golibrdf.Parser
    model *golibrdf.Model
}

func newContext(uri string) librdfContext {
    var err error
	var storage *golibrdf.Storage

	world := golibrdf.NewWorld()

	if err = world.Open(); err != nil {
		log.Panicf("World failed to open: %s", err.Error())
	}

	if storage, err = golibrdf.NewStorage(world, "memory", "test", ""); err != nil {
		log.Panicf("Failed to create storage: %s", err.Error())
	}

    uriBlob, err := golibrdf.NewUri(world, uri)
    if err != nil { log.Panicf("Failed to create a new URI: %s", err.Error()) }

    context := librdfContext{world, storage, uriBlob, nil, nil}
    fmt.Printf("pre %#v\n", context)
    return context
}

func (ctx librdfContext) stringToModel(data string, uri string) *golibrdf.Model {
    fmt.Printf("%s %s\n", uri, data[0:20])
    fmt.Printf("fun %#v\n", ctx)

    guesstimate := golibrdf.GuessName(ctx.world, "", data, "")

    parser, err := golibrdf.NewParser(ctx.world, guesstimate, "")
    if err != nil { log.Panicf("Failed to create a new parser: %s", err.Error()) }

    model, err := golibrdf.NewModel(ctx.world, ctx.storage, "")
    if err != nil { log.Panicf("Failed to create a new model: %s", err.Error()) }

    uriType, err := golibrdf.NewUri(ctx.world, uri)
    err = parser.ParseStringIntoModel(data, uriType, model)
    if err != nil { log.Panicf("Failed to parse %s into the model: %s", guesstimate, err.Error()) }

    return model
}
