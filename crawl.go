package main

import (
    "flag"
    "fmt"
    "io/ioutil"
    "net/http"
    "net/url"
    "os"

    "github.com/PuerkitoBio/fetchbot"
    "rjp.is/golibrdf"
)

var whitelistedTypes = map[string]bool{
    "application/rdf+xml": true,
    "application/x-turtle": true,
    "text/rdf+n3": true,
}

var (
    flagSameHost bool
)

var lctx librdfContext

func main() {
    flag.BoolVar(&flagSameHost, "samehost", false, "Restrict to the initial host")
    flag.Parse()

    lctx = newContext("http://rjp.is")
    fmt.Printf("ret %#v\n", lctx)

    f := fetchbot.New(fetchbot.HandlerFunc(handler))
    queue := f.Start()
    for _, u := range os.Args {
        queue.SendStringGet(u)
    }
    queue.Block()

    // THIS IS A PAIN
    _ = lctx
}

func handler(ctx *fetchbot.Context, res *http.Response, err error) {
    if err != nil {
        fmt.Printf("error: %s\n", err)
        return
    }
    cT := res.Header.Get("Content-Type")

    if _, ok := whitelistedTypes[cT]; ok {
        fmt.Printf("+ [%d] %s %s %s\n", res.StatusCode, ctx.Cmd.Method(), ctx.Cmd.URL(), cT)
        defer res.Body.Close()
        body, err := ioutil.ReadAll(res.Body)
        if err != nil {
            fmt.Printf("error: %s\n", err)
            return
        }
        u := ctx.Cmd.URL().String()
        m := lctx.stringToModel(string(body), u)
        lctx.model = m
        fmt.Printf(m.ToString())
        for _, uri := range enqueueSubjects(lctx, u) {
            if uri != "" {
                _, err := ctx.Q.SendStringGet(uri)
                if err != nil { panic(err) }
            }
        }
    } else {
        fmt.Printf("- [%d] %s %s %s\n", res.StatusCode, ctx.Cmd.Method(), ctx.Cmd.URL().String(), cT)
    }
}

func enqueueSubjects(lctx librdfContext, uri string) []string {
    retval := []string{}

    qS, err := golibrdf.NewStatement(lctx.world)
    if err != nil { panic(err) }

    // Convert our string URI into a URL type
    parentURL, err := url.Parse(uri)
    if err != nil { panic(err) }

    chStatements := lctx.model.FindStatements(qS, 5)

    for {
        stFound := <- chStatements
        if stFound == nil { break }

        retval = append(retval, maybeEnqueue(stFound.GetSubject(), parentURL))
        retval = append(retval, maybeEnqueue(stFound.GetPredicate(), parentURL))
        retval = append(retval, maybeEnqueue(stFound.GetObject(), parentURL))
    }

    return retval
}

func maybeEnqueue(node *golibrdf.Node, parent *url.URL) string {
    // If it's not a resource, ignore this node
    if ! node.IsResource() {
        return ""
    }

    uri := node.GetUriString()
    if uri != "" {
        queue := true
        urlType, err := url.Parse(uri)
        if err != nil { panic(err) }
        if flagSameHost {
            if urlType.Host != parent.Host {
                queue = false
            }
        }

        fmt.Printf("ENQ? %b %s\n", queue, uri)
        if queue {
            return uri
        } else {
            return ""
        }
    }
    return ""
}
